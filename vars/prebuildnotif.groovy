#!/usr/bin/env groovy

def call(ghcatapi,status,emailTo){

     mail bcc: '', body: "THE JOB HAS ${status} \nSTARTED: Job ${env.JOB_NAME} \nBUILD NUMBER: ${env.BUILD_NUMBER} \nJOB URL : ${env.BUILD_URL} \nTIME OF BUILD : ${env.BUILD_TIMESTAMP} \nSTARTED BY : ${env.BUILD_USER}", from: '', replyTo: '', subject: 'job finished successfully', to: "${emailTo}"
        
    googlechatnotification url: "${ghcatapi}",
message: "THE JOB HAS ${status} \nBUILD NUMBER: ${env.BUILD_NUMBER} \nJOB URL : ${env.BUILD_URL} \nTIME OF BUILD : ${env.BUILD_TIMESTAMP} \nSTARTED BY : ${env.BUILD_USER} \nBUILD" 
        }
